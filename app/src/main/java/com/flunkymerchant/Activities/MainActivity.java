package com.flunkymerchant.Activities;

import android.app.Activity;
import android.content.Intent;

import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;

import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.flunkymerchant.Models.ResponseOfAllApiModel;
import com.flunkymerchant.Models.UserVoucherDetails;
import com.flunkymerchant.R;
import com.flunkymerchant.utils.Constant;
import com.flunkymerchant.utils.Logger;
import com.flunkymerchant.utils.Utils;
import com.flunkymerchant.webservices.RestClient;
import com.flunkymerchant.webservices.RetrofitCallback;
import com.google.gson.JsonObject;

import retrofit2.Call;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    private ScrollView fragment_main_ll_user_info;
    private LinearLayout activity_main_ll_options;
    private TextView fragment_main_tv_user_name;
    private TextView fragment_main_tv_unique_id;
    private TextView fragment_main_tv_original_amount;
    private TextView fragment_main_tv_redeem_date;
    private TextView fragment_main_tv_redeem_to_date;
    private TextView fragment_main_tv_remaining_amount;
    private TextView fragment_main_tv_dob;
    private TextView fragment_main_tv_mobile_number;
    private TextView fragment_main_tv_email;

    private ImageView activity_main_iv_user_info_close;
    private ImageView activity_main_iv_redeem_close;
    private ImageView activity_main_iv_manual_close;

    private LinearLayout fragment_main_ll_redeem;
    private EditText fragment_main_redeem_tv_amount;
    private TextView fragment_main_redeem_tv_submit;
    private TextView fragment_main_redeem_tv_error_message;

    private LinearLayout activity_main_ll_manual_view;
    private TextView activity_main_manual_tv_enter_code;
    private TextView activity_main_manual_tv_submit;
    private TextView activity_main_manual_tv_erro_msg;


    private TextView fragment_main_tv_redeem;
    private TextView activity_main_tv_scan_now;

    private TextView activity_main_tv_manually_enter_card;
    private TextView activity_main_tv_support;
    private UserVoucherDetails userVoucherDetail;

    private long mLastClickTime = 0;
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.activity_main_toolbar);
        setSupportActionBar(myToolbar);

        final String manual_text = "<font color=#ffffff>MANUALLY ENTER</font> <font color=#00d17d>FLUNKY</font>  <font color=#ffffff>GIFT CARD</font>";
        final String flunkySupport_text = " <font color=#00d17d>FLUNKY</font>  <font color=#ffffff>SUPPORT</font>";

        fragment_main_ll_user_info = (ScrollView)findViewById(R.id.fragment_main_ll_user_info);
        activity_main_ll_options=(LinearLayout)findViewById(R.id.activity_main_ll_options);
        fragment_main_tv_user_name = (TextView)findViewById(R.id.fragment_main_tv_user_name);
        fragment_main_tv_unique_id = (TextView)findViewById(R.id.fragment_main_tv_unique_id);
        fragment_main_tv_original_amount = (TextView)findViewById(R.id.fragment_main_tv_originalValue);
        fragment_main_tv_redeem_date = (TextView)findViewById(R.id.fragment_main_tv_redeemDate);
        fragment_main_tv_redeem_to_date = (TextView)findViewById(R.id.fragment_main_tv_redeemToDate);
        fragment_main_tv_remaining_amount = (TextView)findViewById(R.id.fragment_main_tv_remainingAmount);
        fragment_main_tv_dob = (TextView)findViewById(R.id.fragment_main_tv_dob);
        fragment_main_tv_mobile_number = (TextView)findViewById(R.id.fragment_main_tv_movileNumber);
        fragment_main_tv_email = (TextView)findViewById(R.id.fragment_main_tv_email);
        fragment_main_tv_redeem = (TextView)findViewById(R.id.fragment_main_tv_redeem);

        fragment_main_ll_redeem = (LinearLayout)findViewById(R.id.fragment_main_ll_redeem);
        fragment_main_redeem_tv_amount = (EditText)findViewById(R.id.fragment_main_redeem_tv_amount);
        fragment_main_redeem_tv_submit = (TextView)findViewById(R.id.fragment_main_redeem_tv_submit);
        fragment_main_redeem_tv_error_message = (TextView)findViewById(R.id.fragment_main_redeem_tv_error);

        activity_main_ll_manual_view=(LinearLayout)findViewById(R.id.activity_main_manual_view);
        activity_main_manual_tv_enter_code=(TextView)findViewById(R.id.activity_main_manually_tv_code);
        activity_main_manual_tv_submit=(TextView)findViewById(R.id.activity_main_manually_tv_submit);
        activity_main_manual_tv_erro_msg=(TextView)findViewById(R.id.activity_main_manually_tv_error);

        activity_main_iv_user_info_close=(ImageView)findViewById(R.id.activity_main_iv_user_info_close);
        activity_main_iv_redeem_close=(ImageView)findViewById(R.id.activity_main_iv_redeem_close);
        activity_main_iv_manual_close=(ImageView)findViewById(R.id.activity_main_iv_manual_close);

         activity_main_tv_scan_now = (TextView)findViewById(R.id.activity_main_tv_scan_now);

        activity_main_tv_manually_enter_card = (TextView) findViewById(R.id.activity_main_tv_manually_enter);
        activity_main_tv_support = (TextView)findViewById(R.id.activity_main_tv_flunky_support);


        activity_main_tv_manually_enter_card.setText(Html.fromHtml(manual_text));
        activity_main_tv_support.setText(Html.fromHtml(flunkySupport_text));

        fragment_main_redeem_tv_submit.setOnClickListener(this);
        fragment_main_tv_redeem.setOnClickListener(this);
        activity_main_tv_scan_now.setOnClickListener(this);

        activity_main_tv_manually_enter_card.setOnClickListener(this);
        activity_main_manual_tv_submit.setOnClickListener(this);
        activity_main_tv_support.setOnClickListener(this);

        activity_main_iv_manual_close.setOnClickListener(this);
        activity_main_iv_redeem_close.setOnClickListener(this);
        activity_main_iv_user_info_close.setOnClickListener(this);
    }
    private void doValidation() {
        final String amount = fragment_main_redeem_tv_amount.getText().toString();
        String oriAmnt = userVoucherDetail.getOriginalAmount();
        oriAmnt = oriAmnt.replace("$", "").replace(".00","");
        String remainingAmnt = userVoucherDetail.getRemainingAmount();
        remainingAmnt = remainingAmnt.replace("$", "").replace(".00","");
        final int originalAmount = Integer.parseInt(oriAmnt);
        final int remainingAmount = Integer.parseInt(remainingAmnt);
        if(!TextUtils.isEmpty(amount)){
            final int Amount = Integer.parseInt(amount);
            if (Amount > originalAmount) {
                fragment_main_redeem_tv_error_message.setText(getString(R.string.invalid_voucher_value) + userVoucherDetail.getOriginalAmount());
            } else if (Amount > remainingAmount) {
                fragment_main_redeem_tv_error_message.setText(getString(R.string.invalid_remaining_value) + userVoucherDetail.getRemainingAmount());
            } else {
                fragment_main_ll_redeem.setVisibility(View.GONE);
                fragment_main_redeem_tv_error_message.setVisibility(View.GONE);
                doRedeem(amount);
            }
        }else{
            fragment_main_redeem_tv_error_message.setText(R.string.enter_amount);
        }

    }
    private void getVoucherDetail(String rawValue) {
        Call<ResponseOfAllApiModel> getVoucherDetail = new RestClient().getApiInterface().getVoucherDetail(Constant.APP_KEY, rawValue, "and");
        getVoucherDetail.enqueue(new RetrofitCallback<ResponseOfAllApiModel>(this, Logger.showProgressDialog(this)) {
            @Override
            public void onSuccess(ResponseOfAllApiModel data) {
                if (data.getUserVoucherDetails() != null) {
                    setBackgroundView();
                    activity_main_tv_scan_now.setVisibility(View.GONE);
                    userVoucherDetail = data.getUserVoucherDetails();
                    setUserVoucherData(userVoucherDetail);
                }
            }
            @Override
            public void onFailure(Call<ResponseOfAllApiModel> call, Throwable error) {
                super.onFailure(call, error);
                removeBackgroundView();
                fragment_main_ll_user_info.setVisibility(View.GONE);

                activity_main_tv_scan_now.setVisibility(View.VISIBLE);
                        }
        });
    }
    private void doRedeem(String amount) {
        Call<JsonObject> redeemCall = new RestClient().getApiInterface().doRedeem(Constant.APP_KEY,amount, Utils.getString(this,Constant.User_id),userVoucherDetail.getCid());
        redeemCall.enqueue(new RetrofitCallback<JsonObject>(this, Logger.showProgressDialog(this)) {
            @Override
            public void onSuccess(JsonObject data) {
                Logger.toast(MainActivity.this,data.get("res_message").toString());
                if(data.get("res_code").getAsString().equalsIgnoreCase("1")){
                    removeBackgroundView();
                    fragment_main_ll_user_info.setVisibility(View.GONE);

                    activity_main_tv_scan_now.setVisibility(View.VISIBLE);
                } if(data.get("res_code").getAsString().equalsIgnoreCase("0")){
                    fragment_main_ll_redeem.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 500) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();
        switch (v.getId()) {
            case R.id.activity_main_tv_scan_now:
                Intent i = new Intent(MainActivity.this,ScanActivity.class);
                startActivityForResult(i,200);
                break;
            case R.id.activity_main_tv_manually_enter:
                setBackgroundView();
                 setManualView();
                break;
            case R.id.activity_main_manually_tv_submit:
                activity_main_manual_tv_erro_msg.setText("");
                Utils.hideSoftKeyboard(this);
                doValidationForManualView();
                break;
            case R.id.activity_main_tv_flunky_support:
                callMailClient();
                break;
            case R.id.fragment_main_tv_redeem:
                fragment_main_ll_redeem.setVisibility(View.VISIBLE);
                fragment_main_redeem_tv_error_message.setVisibility(View.VISIBLE);
                activity_main_iv_user_info_close.setVisibility(View.GONE);
                fragment_main_redeem_tv_error_message.setText("");
                fragment_main_redeem_tv_amount.setText("");
                break;
            case R.id.fragment_main_redeem_tv_submit:
                Utils.hideSoftKeyboard(this);
                doValidation();
                break;
            case R.id.activity_main_iv_manual_close:
                Utils.hideSoftKeyboard(this);
                hideManualView();
                removeBackgroundView();
                break;
            case R.id.activity_main_iv_redeem_close:
                Utils.hideSoftKeyboard(this);
                hideRedeemView();
                break;
            case R.id.activity_main_iv_user_info_close:
                hideUserInfoView();
                removeBackgroundView();
                break;

        }
    }

    private void callMailClient() {
        if(Utils.isMailClientPresent(this)){
            final String []To={"yourvoucher@flunky.com"};
            Intent voucheremailIntent = new Intent(Intent.ACTION_SEND);
            voucheremailIntent.setType("text/plain");
            voucheremailIntent.putExtra(Intent.EXTRA_EMAIL,To);
            voucheremailIntent.putExtra(Intent.EXTRA_SUBJECT, "Contact For Voucher Information");
            startActivity(voucheremailIntent);
        }else{
            Toast.makeText(this, R.string.install_mail_client,Toast.LENGTH_SHORT).show();
        }
    }

    private void setBackgroundView() {
        activity_main_ll_options.setBackgroundColor(getResources().getColor(R.color.gray));

        activity_main_tv_manually_enter_card.setOnClickListener(null);
        activity_main_tv_support.setOnClickListener(null);
    }
    private void removeBackgroundView(){
        activity_main_ll_options.setBackgroundColor(getResources().getColor(R.color.black));

        activity_main_tv_manually_enter_card.setOnClickListener(this);
        activity_main_tv_support.setOnClickListener(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 200) {
            if(resultCode == Activity.RESULT_OK){
                String code=data.getStringExtra("result");

                activity_main_tv_scan_now.setVisibility(View.GONE);
                getVoucherDetail(code);
            }
        }
    }
    private void doValidationForManualView() {
        final String code=activity_main_manual_tv_enter_code.getText().toString();
        if(!TextUtils.isEmpty(code)){
            getVoucherDetail(code);
        }else{
            activity_main_manual_tv_erro_msg.setText(R.string.enter_coupan_code);
        }
    }
    private void setManualView() {
        fragment_main_ll_user_info.setVisibility(View.GONE);
        activity_main_tv_scan_now.setVisibility(View.GONE);
        activity_main_ll_manual_view.setVisibility(View.VISIBLE);

    }
    private void setUserVoucherData(UserVoucherDetails userVoucherDetails) {
        fragment_main_tv_user_name.setText(userVoucherDetails.getUsername());
        fragment_main_tv_unique_id.setText(userVoucherDetails.getRedeemCode().toUpperCase());
        fragment_main_tv_original_amount.setText(userVoucherDetails.getOriginalAmount());
        fragment_main_tv_redeem_date.setText(userVoucherDetails.getRedeemDate());
        fragment_main_tv_redeem_to_date.setText(userVoucherDetails.getRedeem_to_date());
        fragment_main_tv_remaining_amount.setText(userVoucherDetails.getRemainingAmount());
        fragment_main_tv_dob.setText(userVoucherDetails.getBod());
        fragment_main_tv_mobile_number.setText(userVoucherDetails.getMobileNumber());
        fragment_main_tv_email.setText(userVoucherDetails.getEmail());
        fragment_main_ll_user_info.setVisibility(View.VISIBLE);
        activity_main_ll_manual_view.setVisibility(View.GONE);
        activity_main_manual_tv_erro_msg.setText("");
    }
    private void hideManualView(){
        fragment_main_ll_user_info.setVisibility(View.GONE);
        activity_main_tv_scan_now.setVisibility(View.VISIBLE);
        activity_main_ll_manual_view.setVisibility(View.GONE);
        activity_main_manual_tv_erro_msg.setText("");

    }
    private void hideRedeemView(){
        fragment_main_ll_redeem.setVisibility(View.GONE);
        fragment_main_redeem_tv_error_message.setVisibility(View.GONE);
        fragment_main_redeem_tv_error_message.setText("");
        fragment_main_redeem_tv_amount.setText("");
        activity_main_iv_user_info_close.setVisibility(View.VISIBLE);
    }
    private void hideUserInfoView(){
        fragment_main_ll_user_info.setVisibility(View.GONE);
        activity_main_ll_manual_view.setVisibility(View.GONE);
        activity_main_manual_tv_erro_msg.setText("");

        activity_main_tv_scan_now.setVisibility(View.VISIBLE);
    }
}
