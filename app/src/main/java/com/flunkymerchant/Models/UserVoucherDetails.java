package com.flunkymerchant.Models;


import com.google.gson.annotations.SerializedName;

public class UserVoucherDetails {

    @SerializedName("user_name")
    private String username;

    @SerializedName("cid")
    private String cid;

    @SerializedName("user_id")
    private String user_id;

    @SerializedName("original_amount")
    private String originalAmount;

    @SerializedName("redeem_code")
    private String redeemCode;

    @SerializedName("redeem_date")
    private String redeemDate;

    @SerializedName("redeem_to_date")
    private String redeem_to_date;

    @SerializedName("remaining_amount")
    private String remainingAmount;

    @SerializedName("bod")
    private String bod;

    @SerializedName("mobileNumber")
    private String mobileNumber;

    @SerializedName("email")
    private String email;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getOriginalAmount() {
        return originalAmount;
    }

    public void setOriginalAmount(String originalAmount) {
        this.originalAmount = originalAmount;
    }

    public String getRedeemCode() {
        return redeemCode;
    }

    public void setRedeemCode(String redeemCode) {
        this.redeemCode = redeemCode;
    }

    public String getRedeemDate() {
        return redeemDate;
    }

    public void setRedeemDate(String redeemDate) {
        this.redeemDate = redeemDate;
    }

    public String getRedeem_to_date() {
        return redeem_to_date;
    }

    public void setRedeem_to_date(String redeem_to_date) {
        this.redeem_to_date = redeem_to_date;
    }

    public String getRemainingAmount() {
        return remainingAmount;
    }

    public void setRemainingAmount(String remainingAmount) {
        this.remainingAmount = remainingAmount;
    }

    public String getBod() {
        return bod;
    }

    public void setBod(String bod) {
        this.bod = bod;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }



}
