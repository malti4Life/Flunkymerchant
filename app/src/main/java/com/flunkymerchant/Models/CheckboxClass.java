package com.flunkymerchant.Models;

public class CheckboxClass {
    private boolean isChecked=false;
    private boolean isCheckedFull=false;

    public boolean isCheckedFull() {
        return isCheckedFull;
    }

    public void setCheckedFull(boolean checkedFull) {
        isCheckedFull = checkedFull;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
