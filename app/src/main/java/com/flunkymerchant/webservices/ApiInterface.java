package com.flunkymerchant.webservices;


import com.flunkymerchant.Models.ResponseOfAllApiModel;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ApiInterface {

    @FormUrlEncoded
    @POST("index.php/applogin")
    Call<ResponseOfAllApiModel> doLogin(@Field("login")String email,  @Field("password")String password,
                                       @Field("apikey")String apiKey, @Field("deviceid")String device_id,
                                        @Field("devicetoken")String device_token, @Field("os")String os);
    @FormUrlEncoded
    @POST("index.php/getAppVoucherDetails")
    Call<ResponseOfAllApiModel> getVoucherDetail( @Field("apikey")String apiKey, @Field("v_code")String coupan_code,
                                                  @Field("os")String os);

    @FormUrlEncoded
    @POST("index.php/appMerchantRedeemVouchers")
    Call<JsonObject> doRedeem(@Field("apikey")String apiKey, @Field("redeem_amount")String redeemAmount,
                              @Field("user_id")String userId, @Field("cid")String cid);
}